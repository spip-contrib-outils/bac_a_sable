<?php

namespace Spip\BacASable;

class Demo
{
    private string $demo = 'demo';

    public function __construct() {}

    public function getDemo(): string
    {
        return $this->demo;
    }

    public function getRepeatedDemo(int $repeat = 1): string
    {
        return str_repeat($this->demo, $repeat);
    }
}
