<?php

declare(strict_types=1);

namespace Spip\BacASable\Test;

use PHPUnit\Framework\TestCase;
use Spip\BacASable\Demo;

class DemoTest extends TestCase
{
    public function testDemo(): void
    {
        $demo = new Demo();
        $this->assertSame($demo->getDemo(), 'demo');
    }

    public function testDemoRepeat(): void
    {
        $demo = new Demo();
        $this->assertSame($demo->getRepeatedDemo(), 'demo');
        $this->assertSame($demo->getRepeatedDemo(3), 'demodemodemo');
    }
}
