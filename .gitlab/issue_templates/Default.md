# Bug
## Informations techniques

- Version de SPIP :
- Version de PHP :
- Serveur web (si pertinent) :
- Système d'exploitation (si pertinent) :
- Base de données (si pertinent) : MySQL/MariaDb/Sqlite/ + numéro de version

## Description du bug / du besoin

## Comment reproduire le bug

## Comportement attendu

## Contournements possibles

## Piste(s) de résolution
