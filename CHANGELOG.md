# Changelog

## 1.3.0 - 2025-01-11

### Added

- #2 Utilisation du trailer

### Fixed

- #3 Une autre entrée

## 1.2.2 - 2025-01-11

## 1.2.1 - 2025-01-11

### Fixed

- #1 Un ticket d’entrée

## 1.2.0 - 2025-01-10

### Added

- Elle est extra
- Une fonctionnalité nouvelle est déployée

### Changed

- Déplacer les fichiers de bac à sable d’écriture dans un répertoire sandbox/

### Fixed

- Une autre chose est fixée
- Quelque chose est fixé
