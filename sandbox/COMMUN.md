# Fichier commun pour essai de git

Test
## Partie introductive

Afin de tester le travail collaboratif avec Git, ce fichier existe. Il peut être modifié par tout le monde.

Des fichiers propres à chaque personne (ex. maieul.md) peuvent être créé. Seule une personne peut modifier ces fichier.

Le présent document sert pour la formation git du 21 novembre 2023, et qui sait pour les futures formations.

## Partie modifiable en simultanée



Cette section est destinée à être modifiée par plusieurs personnes en parallèles, pour voir comment résoudre les problèmes de conflits.

Modif par Jacques (2eme) et Maïeul (qui rattrape le boulot de Jacques) : C'était rien que du faux latin ! Y'en avait marre. Ça ne nous rajeunit pas ! Et Maïeul, c'est une bénédiction pour la communauté SPIP ! Lapin coufin... Cave canem ... !!!

Faisons bosser yohoo : "Lapin de laboratoire ou bugs bunny ?"  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Les deux lignes ci-dessus sont identiques.

## Partie modifiable en décalée

Pour cette section, les gens sont invités à ne pas modifier en même temps qu'une autre personne. Le but est de voir ce qui arrive quand tout se passe bien.

Quel bonheur de giter !

C'est un bonheur pour moi aussi !
